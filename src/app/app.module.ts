import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CustomerDetailsComponent } from './customer/search-details/customer-details.component';
import { SearchCustomersComponent } from './customer/search/search-customers.component';
import { EvalComponent } from './customer/eval/eval.component';
import { EvalDetailsComponent } from './customer/eval-details/eval-details.component';
import { DetailPageComponent } from './customer/detail-page/detail-page.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomerDetailsComponent,
    SearchCustomersComponent,
    EvalComponent, 
    EvalDetailsComponent,
    DetailPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
