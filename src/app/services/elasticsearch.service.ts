import { Injectable } from '@angular/core';
import { Client } from 'elasticsearch-browser';


declare var require: any

@Injectable({
  providedIn: 'root'
})
export class ElasticsearchService {

  private client: Client;

  private queryalldocs = {
    'query': {
      'match_all': {}
    }
  };

  constructor() {
    if (!this.client) {
      this.connect();
    }
  }

  private connect() {
    this.client = new Client({
      host: 'http://localhost:9200',
      log: 'trace'
    });
  }

  isAvailable(): any {
    return this.client.ping({
      requestTimeout: Infinity,
      body: 'hello grokonez!'
    });
  }

  addToIndex(value): any {
    return this.client.create(value);
  }

  getAllDocuments(_index, _type): any {
    return this.client.search({
      index: _index,
      type: _type,
      size : 15,
      body: this.queryalldocs,
      filterPath: ['hits.hits._source']
    });
  }

  getAllDocumentsWithScroll(_index, _size): any {
    return this.client.search({
      index: _index,
      scroll: '1m',
      filterPath: ['hits.hits._source', 'hits.total', '_scroll_id'],
      body: {
        'size': _size,
        'query': {
          'match_all': {}
        },
      }
    });
  }

  getNextPage(scroll_id): any {
    return this.client.scroll({
      scrollId: scroll_id,
      scroll: '1m',
      filterPath: ['hits.hits._source', 'hits.total', '_scroll_id']
    });
  }

  fullTextSearch(_index, _field, _queryText): any {
    return this.client.search({
      index: _index,
      body: {
        'query': {
          'match_phrase_prefix': {
            [_field]: _queryText,
          }
        }
      }
    });
  }


  findTweetById(id) : any{
    var esb = require('elastic-builder'); // the builder
    var requestBody = esb.requestBodySearch()
    .query(esb.matchQuery("id_str", id))  
    return this.client.search({
      index: "trump-search",
      body: requestBody.toJSON()
    });
  }

  fuzzyTextSearch(_index, _field, _queryText): any {
    var esb = require('elastic-builder'); // the builder
    

    const highlight = esb.highlight()
    .numberOfFragments(3)
    .fragmentSize(150)
    .fields([_field])
    .preTags('<span class="match">', _field)
    .postTags('</span>', _field);


    
let x = _queryText.split(" ");
var numberOfWords = x.length;
var minimumShouldMatch : number = (Math.round(numberOfWords/2));
console.log(minimumShouldMatch);

var  sw = require('stopword')

sw = require('stopword')
const queryWithoutStoppwords = sw.removeStopwords(x)
console.log(_queryText);
console.log(queryWithoutStoppwords.join());


    var requestBody = esb.requestBodySearch()
    .query(esb.matchQuery(_field, queryWithoutStoppwords.join()).fuzziness(1).fuzzyRewrite("constant_score").minimumShouldMatch(minimumShouldMatch)).highlight(highlight).size(15);
    

   

    
    return this.client.search({
      index: _index,
      body: requestBody.toJSON()
  
    });
  }

}
