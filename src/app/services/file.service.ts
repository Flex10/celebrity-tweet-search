import { Injectable } from '@angular/core';
import { saveAs, encodeBase64 } from '@progress/kendo-file-saver';


@Injectable({
  providedIn: 'root'
})
export class FileService {


  constructor() {

  }

  saveJsonToFile(data: any, filename : any) {
    const dataURI = "data:json/plain;base64," + encodeBase64(JSON.stringify(data));
    saveAs(dataURI, filename+".json");
  }


}
