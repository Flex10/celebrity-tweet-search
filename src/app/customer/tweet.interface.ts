export interface Tweet {
    Tweet_Text: string;
    age: number;
    Tweet_Url: string;
    Date: string;
    Time: string;
    highlight: string;
}

export interface TweetSource {
    source: Tweet;
}
