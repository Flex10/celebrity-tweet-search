import { Component, OnInit, Input } from '@angular/core';
import { Tweet, TweetSource } from '../tweet.interface';
import { ActivatedRoute } from '@angular/router';
import { ElasticsearchService } from 'src/app/services/elasticsearch.service';

@Component({
  selector: 'detail-page',
  templateUrl: './detail-page.html',
  styleUrls: ['./detail-page.css']
})
export class DetailPageComponent implements OnInit {
    
  public tweet_id: string;
  public tweet : TweetSource;

  private elasticSearchService : ElasticsearchService;

  constructor(private route: ActivatedRoute, _elasticSearchService : ElasticsearchService) {
    this.elasticSearchService = _elasticSearchService;
    
    this.route.params.subscribe(params => {
      console.log(params) //log the entire params object
      console.log(params['id']) //log the value of id
      this.tweet_id = params['id'];
      console.log("Object");
      this.elasticSearchService.findTweetById(this.tweet_id).then(
        response => {
         this.tweet= response.hits.hits[0]._source;
         console.log(this.tweet);
        }, error => {
          console.error(error);
        }).then(() => {
          console.log('Search Completed!');
        });
    });
   }

  ngOnInit() {
  }

}
