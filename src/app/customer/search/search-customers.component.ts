import { Component, OnInit } from '@angular/core';
import { TweetSource } from '../tweet.interface';
import { ElasticsearchService } from 'src/app/services/elasticsearch.service';

@Component({
  selector: 'app-search-customers',
  templateUrl: './search-customers.component.html',
  styleUrls: ['./search-customers.component.css']
})
export class SearchCustomersComponent implements OnInit {
  //private static readonly INDEX = 'celebrity-tweet-search';
  private static readonly INDEX = 'trump-search';


  tweetSources: TweetSource[] = [];
  private queryText = '';

  private lastKeypress = 0;

  constructor(private es: ElasticsearchService) {
    this.queryText = '';
  }

  ngOnInit() {
    this.es.getAllDocuments(SearchCustomersComponent.INDEX, "_doc").then(
      response => {
        console.log(response.hits.hits);
        response.hits.hits.forEach(hit => {
          hit._source.Time=hit._source["@timestamp"];
          this.tweetSources.push(hit._source);
        });
        console.log(this.tweetSources);
      }, error => {
        console.error(error);
      }).then(() => {
        console.log('All Documents!');
      });
  }

  search($event) {
    if ($event.timeStamp - this.lastKeypress > 100) {
      this.queryText = $event.target.value;
      this.tweetSources = [];
      this.es.fuzzyTextSearch(
        SearchCustomersComponent.INDEX,
        'Tweet_Text', this.queryText).then(
          response => {
            response.hits.hits.forEach(hit => {
              hit._source.highlight=hit.highlight.Tweet_Text;
              hit._source.Time=hit._source["@timestamp"];
              this.tweetSources.push(hit._source);
            });
            console.log(this.tweetSources);
          }, error => {
            console.error(error);
          }).then(() => {
            console.log('Search Completed!');
          });
    }

    this.lastKeypress = $event.timeStamp;
  }
}
