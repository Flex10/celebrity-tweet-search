import { Component, OnInit, Input } from '@angular/core';
import { Tweet } from '../tweet.interface';

@Component({
  selector: 'customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {

  @Input() tweet: Tweet;



  constructor() {
   }

  ngOnInit() {
  }

}
