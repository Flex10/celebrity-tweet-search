import { Component, OnInit } from '@angular/core';
import { TweetSource } from '../tweet.interface';
import { ElasticsearchService } from 'src/app/services/elasticsearch.service';
import { FileService } from 'src/app/services/file.service';

declare var require: any;

@Component({
  selector: 'eval-container',
  templateUrl: './eval.html',
  styleUrls: ['./eval.css']
})
export class EvalComponent implements OnInit {
  private static readonly INDEX = 'trump-search';


  public tweetSources: TweetSource[] = [];
  public selectedTopic;
  public topics;
  private queryText = '';

  private lastKeypress = 0;
  private fileService: FileService;
  

  constructor(private es: ElasticsearchService, private _fileService: FileService) {
    this.queryText = '';
    this.fileService = _fileService;
  }

  ngOnInit() {
    this.es.getAllDocuments(EvalComponent.INDEX, "_doc").then(
      response => {
        console.log(response.hits.hits);
        response.hits.hits.forEach(hit => {
          this.tweetSources.push(hit._source);
          hit._source.Time=hit._source["@timestamp"];
        });
        console.log(this.tweetSources);
      }, error => {
        console.error(error);
      }).then(() => {
        console.log('All Documents!');
      });
    this.loadTopicJson();
    console.log(this.topics);
  }
  loadTopicJson() {
    console.log("File load...");
    this.topics = require('src/file.json');
  }

downloadFile(){
  var data={};
  var summary = {};
  var counter = 1;
  summary = this.calcSummary();
  data["summary"] = summary
  this.tweetSources.forEach(x=>{
    var judgment={};
    judgment["tweet_id"]=x["id_str"];
    judgment["relevant"]=x["relevant"];
    judgment["rank"]=counter;
    data["judgments"+counter] = judgment;
    counter ++;
  });

  this.fileService.saveJsonToFile(data, this.topics.find(x => x.query == this.selectedTopic).topic_id);
}

calcSummary(): any{
  var relevant = 0;
  var averragePrecision = 0;
  this.tweetSources.forEach(x=>{
    if (x["relevant"]){
      relevant ++;
      x["precision"]=relevant/x["precision"]
      averragePrecision= averragePrecision +  x["precision"];
    }
  });
  var summary = {};
  summary["topic"]= this.selectedTopic;
  summary["results"]= this.tweetSources.length; 
  summary["precision"]= relevant/this.tweetSources.length; 
  summary["averragePrecision"]= relevant/this.tweetSources.length;  
  return summary;
}

  search() {
      this.queryText = this.selectedTopic;
      this.tweetSources = [];
      this.es.fuzzyTextSearch(
        EvalComponent.INDEX,
        'Tweet_Text', this.queryText).then(
          
          response => {
            console.log(response);
            response.hits.hits.forEach(hit => {
              hit._source.highlight=hit.highlight.Tweet_Text;
              hit._source.Time=hit._source["@timestamp"];
              this.tweetSources.push(hit._source);
              console.log(this.tweetSources);
            });
            console.log(this.tweetSources);
          }, error => {
            console.error(error);
          }).then(() => {
            console.log('Search Completed!');
          });
  }
}
