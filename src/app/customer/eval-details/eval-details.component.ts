import { Component, OnInit, Input } from '@angular/core';
import { Tweet } from '../tweet.interface';

@Component({
  selector: 'eval-details',
  templateUrl: './eval-details.component.html',
  styleUrls: ['./eval-details.component.css']
})
export class EvalDetailsComponent implements OnInit {

  @Input() tweet: Tweet;



  constructor() {
   }

  ngOnInit() {
  }

}
