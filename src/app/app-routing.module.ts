import { SearchCustomersComponent } from './customer/search/search-customers.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EvalComponent } from './customer/eval/eval.component';
import { DetailPageComponent } from './customer/detail-page/detail-page.component';

const routes: Routes = [
    { path: '', redirectTo: 'search', pathMatch: 'full' },
    { path: 'search', component: SearchCustomersComponent },
    { path: 'eval', component: EvalComponent },
    { path: 'tweet/:id', component: DetailPageComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
